﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlayerInput : MonoBehaviour
{
    GamePlay gamePlayObject;
    Game currentGame;

    // Start is called before the first frame update
    void Start()
    {
        gamePlayObject = GameObject.Find("Gameplay").GetComponent<GamePlay>();
        currentGame = gamePlayObject.game;
    }

    public void StartButtonOnClick()
    {
        bool isTotalValid = Int32.TryParse(gamePlayObject.totalAmount.text, out int total);
        bool isBetValid = Int32.TryParse(gamePlayObject.betAmount.text, out int bet);
        
        // check bet to be a valid value
        if (isBetValid && isTotalValid && bet <= total)
        {
            gamePlayObject.startNew(bet, gamePlayObject.GetTotalCards());
            EventSystem.current.currentSelectedGameObject.GetComponent<Button>().interactable = false;
            GameObject.Find("SurrenderButton").GetComponent<Button>().interactable = true;
        }
        else
        {
            gamePlayObject.messageText.text = "Not a valid bet";
            throw new Exception("Not a valid bet");
        }

    }
    public void HitButtonOnClick()
    {
        // player draw a card
        Card cardJustGet = currentGame.ReceiveACard("player");
        gamePlayObject.SpawnACard("player", currentGame.getPlayerHand().Count - 1, false, cardJustGet.getRank(), cardJustGet.getSuit());

        // check if bust
        if( currentGame.isBust(currentGame.getPlayerHand()) )
        {
            gamePlayObject.endTurn(-1, false);
        }

        if( currentGame.getPlayerHand().Count > 2 )
        {
            GameObject.Find("SurrenderButton").GetComponent<Button>().interactable = false;
            GameObject.Find("DoubleButton").GetComponent<Button>().interactable = false;
        }
    }
    public void StandButtonOnClick()
    {
        GameObject.Find("HitButton").GetComponent<Button>().interactable = false;
        
        // Dealer draw cards until 17 or over
        while ( currentGame.CalculateTotalInHand(currentGame.getDealerHand()) < 17 )
        {
            Card cardJustGet = currentGame.ReceiveACard("dealer");
            gamePlayObject.SpawnACard("dealer", currentGame.getDealerHand().Count - 1, false, cardJustGet.getRank(), cardJustGet.getSuit());
            if (currentGame.isBust(currentGame.getDealerHand()) )
            {
                gamePlayObject.endTurn(1, false);
                return;
            }
        }

        // Show the result!
        gamePlayObject.endTurn(currentGame.RoundJudge(), false);

    }
    public void DoubleButtonOnClick()
    {
        if ( Int32.TryParse(gamePlayObject.totalAmount.text, out int total) )
        {
            // Not enought to double a bet then bet total!
            if (currentGame.getBet() * 2 <= total)
            {
                currentGame.setBet(currentGame.getBet() * 2);
                
            }
            else
            {
                currentGame.setBet(total);
            }

            gamePlayObject.betAmount.text = currentGame.getBet().ToString();
            EventSystem.current.currentSelectedGameObject.GetComponent<Button>().interactable = false;
        }
        else
        {        
            gamePlayObject.messageText.text = "Not a valid bet";
            throw new Exception("Not a valid bet");
        }
        
        
        
    }
    public void SurrenderButtonOnClick()
    {
        gamePlayObject.endTurn(-1, true);
    }
}
