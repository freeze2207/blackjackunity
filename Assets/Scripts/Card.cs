﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
public class Card
{
    private int suit = 0;
    private int rank = 0;
    private Sprite cardBack = Resources.Load<Sprite>("Sprites/card_back");

    public Card(int suit, int rank)
    {
        this.suit = suit;
        this.rank = rank;
    }

    public int getSuit()
    {
        return this.suit;
    }
    public int getRank()
    {
        return this.rank;
    }
    
}
