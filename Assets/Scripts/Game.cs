﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class Game
{
    private int currentBet = 0;
    private List<Card> dealer;
    private List<Card> player;
    private List<Card> totalCardsUsedInThisGame;

    public Game()
    {
        this.currentBet = 0;
        dealer = new List<Card>();
        player = new List<Card>();
    }
    public void ResetGame()
    {
        this.currentBet = 0;
        dealer.Clear();
        player.Clear();
        totalCardsUsedInThisGame.Clear();
    }

    public int getBet()
    {
        return this.currentBet;
    }
    public void setBet(int bet)
    {
        this.currentBet = bet;
    }

    public void setTotalCards(List<Card> cards)
    {
        totalCardsUsedInThisGame = new List<Card>(cards);
    }
    public List<Card> getPlayerHand()
    {
        return this.player;
    }

    public List<Card> getDealerHand()
    {
        return this.dealer;
    }

    public Card ReceiveACard(string side)
    {
        Card cardToGet = DrawACard(totalCardsUsedInThisGame);
        if (side == "dealer")
        {
            dealer.Add(cardToGet);

        }else if (side == "player")
        {
            player.Add(cardToGet);

        }else
        {
            Debug.LogError("Invalid side name to receive the card");
        }
        return cardToGet;
    }
    
    public Card DrawACard(List<Card> totalCards)
    {

        System.Random random = new System.Random(DateTime.Now.Millisecond);

        int luckyNumber = random.Next(0, totalCards.Count);

        // protect drawing card
        try
        {
            Card cardDrawed = totalCards[luckyNumber];

            totalCards.RemoveAt(luckyNumber);

            return cardDrawed;
        }
        catch (Exception ex)
        {
            Debug.LogError("Drawed an invalid card");
            throw ex;
        }

    }
    public bool isBust(List<Card> list)
    {
        return CalculateTotalInHand(list) > 21 ? true : false;
    }

    public int CalculateTotalInHand(List<Card> list)
    {
        int aceInhand = 0;
        int sum = 0;

        //determind the original value
        for (int i = 0, value = 0; i < list.Count; i++)
        {

            if (list[i].getRank() > 9)   // J, Q, K
            {
                value = 10;
            }
            else
            {
                value = list[i].getRank() + 1;
            }
            if (list[i].getRank() == 0)     // Aces value 11 first
            {
                aceInhand++;
                value += 10;
            }

            sum += value;
        }
        // decide Ace value 1 or 11
        for (int j = 0; j < aceInhand; j++)
        {
            if (sum > 21)
            {
                sum -= 10;
            }
        }

        if( sum == 0) { Debug.Log("No cards in hand"); }

        return sum;
    }
    public int RoundJudge()
    {

        if( CalculateTotalInHand(player) > CalculateTotalInHand(dealer) )
        {
            return 1;
        }else if( CalculateTotalInHand(player) == CalculateTotalInHand(dealer) )
        {
            return 0;
        }else
        {
            return -1;
        }
        
    }

}
